<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Home</title>
</head>
<body>

	<%
		String fname = session.getAttribute("fname").toString();
		String lname = session.getAttribute("lname").toString();
		String accType = session.getAttribute("accType").toString();
		String message;
		
		if(accType.equals("applicant")){
			message="Welcome applicant. You may now start looking for your career opportunity.";
		} else {
			message="Welcome employer. You may now start browsing applicant profiles.";
		}
		
	%> 
	
	<h1>Welcome <%= fname + " " + lname %>!</h1>
	<p><%= message %></p>

</body>
</html>