<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Registration Confirmation</title>
</head>
<body>

	<%
		String accType = session.getAttribute("accType").toString();
		
		if(accType.equals("applicant")){
			accType="Applicant";
		} else {
			accType="Employee";
		}
		
		String birthdate = session.getAttribute("birthDate").toString();
	%> 
	
	<h1>Registration Confirmation</h1>
	<p>First Name: <%= session.getAttribute("fname") %></p>
	<p>Last Name: <%= session.getAttribute("lname") %></p>
	<p>Phone: <%= session.getAttribute("phone") %></p>
	<p>Email: <%= session.getAttribute("email") %></p>
	<p>App Discovery: <%= session.getAttribute("appDiscovery") %>
	<p>Date of Birth: <%= birthdate %>
	<p>User Type: <%= accType %>
	<p>Description: <%= session.getAttribute("profileDesc") %>
	
	<form action="home.jsp" method="post">
		<input type="submit">
	</form>
	
	<form action="index.jsp">
		<input type="submit" value="Back">
	</form>

</body>
</html>