<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Servlet Job Finder</title>

	<style>
		div {
			margin-top: 5px;
			margin-bottom: 5px
		}
	</style>
</head>
<body>
	<h1>Welcome to Servlet Job Finder!</h1>
	
	<form method="post" action="register">
		<div>
			<label for="fname">First Name</label>
			<input type="text" name="fname" required>
		</div>
		
		<div>
			<label for="lanme">Last Name</label>
			<input type="text" name="lname" required>
		</div>
		
		<div>
			<label for="phone">Phone</label>
			<input type="tel" name="phone" required>
		</div>
		
		<div>
			<label for="email">Email</label>
			<input type="email" name="email" required>
		</div>
		
		<fieldset>
			<legend>How did you discover the app?</legend>
			<input type="radio" id="friends" name="app_discovery" required value="friends">
			<label for="taxi">Friends</label>
			<br>
			
			<input type="radio" id="social_media" name="app_discovery" required value="socialmedia">
			<label for="social_media">Social Media</label>
			<br>
			
			<input type="radio" id="others" name="app_discovery" required value="others">
			<label for="others">Others</label>
			<br>
		</fieldset>
		
		<div>
			<label for="date_of_birth">Date of Birth</label>
			<input type="date" name="date_of_birth" required>
		</div>
		
		<div>
			<label for=acc_type>Are you an employer or applicant?</label>
			<select id="type_acc" name="acc_type">
				<option value="" selected="selected">Select One</option>
				<option value="applicant">Applicant</option>
				<option value="employer">Employer</option>
			</select>
		</div>
		
		<div>
			<label for="profile_description">Profile Description</label>
			<textarea name="description" maxlength="500"></textarea>
		</div>
		
		<button>Register</button>
	</form>

</body>
</html>