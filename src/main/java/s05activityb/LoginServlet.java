package s05activityb;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	
	public void init() throws ServletException {
		System.out.println("*************************************");
		System.out.println("RegisterServlet has been initialized.");
		System.out.println("*************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String fname = req.getParameter("fname");
		String lname = req.getParameter("lname");
		//String fullName = "fname + lname";
		String accType = req.getParameter("acc_type");
		
		//Stores all the data from the form into the session
		HttpSession session = req.getSession();
		//session.setAttribute("fullName", fullName);
		session.setAttribute("fname", fname);
		session.setAttribute("lname", lname);
		session.setAttribute("accType", accType);
		
		res.sendRedirect("home.jsp");
	}
	
	public void destroy() {
		System.out.println("***********************************");
		System.out.println("RegisterServlet has been finalized.");
		System.out.println("***********************************");
	}
}
