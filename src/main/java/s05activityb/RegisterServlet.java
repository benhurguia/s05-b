package s05activityb;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
	
	public void init() throws ServletException {
		System.out.println("*************************************");
		System.out.println("RegisterServlet has been initialized.");
		System.out.println("*************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String fname = req.getParameter("fname");
		String lname = req.getParameter("lname");
		String phone = req.getParameter("phone");
		String email = req.getParameter("email");
		String appDiscovery = req.getParameter("app_discovery");
		String birthDate = req.getParameter("date_of_birth");
		String accType = req.getParameter("acc_type");
		String profileDesc = req.getParameter("description");
		
		
		//Stores all the data from the form into the session
		HttpSession session = req.getSession();
		session.setAttribute("fname", fname);
		session.setAttribute("lname", lname);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("appDiscovery", appDiscovery);
		session.setAttribute("birthDate", birthDate);
		session.setAttribute("accType", accType);
		session.setAttribute("profileDesc", profileDesc);
		
		res.sendRedirect("register.jsp");
	}
	
	public void destroy() {
		System.out.println("***********************************");
		System.out.println("RegisterServlet has been finalized.");
		System.out.println("***********************************");
	}
}
